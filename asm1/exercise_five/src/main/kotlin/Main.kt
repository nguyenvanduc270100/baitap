fun main(args: Array<String>) {
    //Viết chương trình nhập tháng và năm, in ra số ngày tháng.

    val a:String? = readLine()
    val b:String? = readLine()
    val nam:Int = a!!.toInt()
    val thang:Int = b!!.toInt()

    result(thang, nam)

}

fun checkNam(nam:Int): Boolean{
    return ((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0)
}

fun result(thang:Int, nam:Int): Int{
    when(thang){
        1,3,5,7,8,10,12 -> return 31
        4,6,9,11 -> return 30
        2-> {if (checkNam(nam)) return 29
            else return 28}
    }
    return 0
}