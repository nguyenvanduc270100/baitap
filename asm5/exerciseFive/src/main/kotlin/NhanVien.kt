class NhanVien {

    var ten: String? = null
        get() = field
        set(value) {field = value}
     var gioiTinh: String? = null
        get() = field
        set(value) {
            field = value
        }
    var namSinh: String? = null
        get() = field
        set(value) {
            field = value
        }
    var maNV: Int  = 0
        get() = field
        set(value) {
            field = value
        }
    var trinhDo: String? = null
        get() = field
        set(value) {
            field = value
        }
    var phone:Int  = 0
        get() = field
        set(value) {
            field = value
        }

    constructor(ten: String?, gioiTinh: String?, namSinh: String?, phone: Int) {
        this.ten = ten
        this.gioiTinh = gioiTinh
        this.namSinh = namSinh
        this.phone = phone
    }

    constructor(ten: String?, gioiTinh: String?, namSinh: String?, maNV: Int, trinhDo: String?, phone: Int) {
        this.ten = ten
        this.gioiTinh = gioiTinh
        this.namSinh = namSinh
        this.maNV = maNV
        this.trinhDo = trinhDo
        this.phone = phone
    }


    override fun toString(): String {
        return "NhanVien(ten=$ten, gioiTinh=$gioiTinh, namSinh=$namSinh, maNV=$maNV, trinhDo=$trinhDo, phone=$phone)"
    }




}