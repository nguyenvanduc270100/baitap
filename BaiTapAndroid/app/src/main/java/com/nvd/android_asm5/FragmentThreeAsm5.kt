package com.nvd.android_asm5

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.nvd.myapplication.R

class FragmentThreeAsm5 : Fragment() {
    var img : ImageView? = null
    var txtName : TextView? = null
    var txtPrice : TextView? = null
    var txtDes : TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_three_asm5, container, false)
        img = view.findViewById(R.id.imgProduct)
        txtName = view.findViewById(R.id.name)
        txtPrice = view.findViewById(R.id.price)
        txtDes = view.findViewById(R.id.description)


        return view
    }
}