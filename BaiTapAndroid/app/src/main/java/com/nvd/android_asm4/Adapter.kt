package com.nvd.android_asm4

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.Adapter
import androidx.recyclerview.widget.RecyclerView
import com.nvd.myapplication.R
import kotlin.coroutines.coroutineContext

class Adapter : RecyclerView.Adapter<com.nvd.android_asm4.Adapter.ViewHodel>{

    private var listProduct = ArrayList<Product>()
    private var ctx : Context? = null

    class ViewHodel(itemView: View) : RecyclerView.ViewHolder(itemView){
        val img = itemView.findViewById<ImageView>(R.id.image)
        val txtName = itemView.findViewById<TextView>(R.id.name)
        val txtPrice = itemView.findViewById<TextView>(R.id.price)
        var layout = itemView.findViewById<LinearLayout>(R.id.layout_item)

    }
    constructor(listProduct : ArrayList<Product>, ctx : Context){
        this.listProduct = listProduct
        this.ctx = ctx
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHodel {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_item, parent, false)
        return ViewHodel(view)
    }

    override fun onBindViewHolder(holder: ViewHodel, position: Int) {
        val product = listProduct.get(position)
        holder.img.setImageResource(product.Label!!)
        holder.txtName.text = product.Name
        holder.txtPrice.text = product.Price
        holder.layout.setOnClickListener {
            Toast.makeText(ctx, "Name: ${product.Name} Price: ${product.Price}", Toast.LENGTH_LONG).show()
        }

    }

    override fun getItemCount(): Int {
        return listProduct.size
    }
}


