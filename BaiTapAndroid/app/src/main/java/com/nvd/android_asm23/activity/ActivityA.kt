package com.nvd.android_asm23.activity

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.nvd.myapplication.R


class ActivityA : AppCompatActivity() {
// bài 2: nhận kết quả trả về và in ra log
//    val mActivityResult = registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
//        if (result.resultCode == Activity.RESULT_OK) {
//            val intent = result.data
//            val ketQua = intent?.getIntExtra("data_back", 0)
//            Log.d("aa", ketQua!!.toString())
//        }
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activity)

        // bài 1
//        val arr = arrayOf("Hello!", "Hi!", "Salut!", "Hallo!", "Ciao!","Ahoj!", "YAHsahs!", "Bog!", "Hej ! "," Czesc!"," Ní! "," Kon'nichiwa! ", " Annyeonghaseyo!", "Shalom!", "Sah-wahd-dee-kah! ", " Merhaba!", " Hujambo! ", " Olá! ")
//        val intent = Intent(this,ActivityB::class.java)
//        val bundle = Bundle()
//        bundle.putStringArray("data", arr)
//        intent.putExtras(bundle)
//        startActivity(intent)

//====================================================

        // bài 2
//        val a : Int  = 5
//        val b : Int  = 5
//        val button = findViewById<Button>(R.id.btnSum)
//        button.setOnClickListener {
//            val inten = Intent(this, ActivityB::class.java)
//            inten.putExtra("number_one", a)
//            inten.putExtra("number_two", b)
//            mActivityResult.launch(inten)
//        }

// ====================================================

        // bài 3

        // cách 1 khởi tạo bằng intent
//        val intent = Intent()
//        intent.component = ComponentName("com.nvd", "com.nvd.myapplication")
//        startActivity(intent)

        // cách 2: khởi tạo bằng broadcast receiver
        // activity A gửi broadcast và mở activity B
        // trường hợp 1 send broadcast trong cùng 1 project
//        val button = findViewById<Button>(R.id.btnSum)
//        button.setOnClickListener {
//            val intent = Intent(this, MyBroadCast::class.java)
//            intent.putExtra("data", "broadcast start activity B")
//            sendBroadcast(intent)
//        }


        // trường hợp 2: send broadcast cho project khác
        // gửi dữ liệu MY_DATA sang project b
        // trong intent filter của project b cũng sẽ đăng ký nhận
        // action là MY_ACTION

//        val MY_ACTION = "com.nvd.DucNV64"
//        val MY_DATA = "DucNV64"
//        val button = findViewById<Button>(R.id.btnSum)
//        button.setOnClickListener {
//            val intent = Intent(MY_ACTION)
//            intent.putExtra(MY_DATA, "broadcast start project B")
//            sendBroadcast(intent)
//        }

        // bài 4, em chưa làm được
    }
}