package com.nvd.android_asm5

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.nvd.myapplication.R

class FragmentOneAsm5 : Fragment() {
    var edtId : EditText? = null
    var edtName : EditText? = null
    var edtPrice : EditText? = null
    var edtDescription : EditText? = null
    var imgProduct : ImageView? = null
    var btnAdd : Button? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view : View = inflater.inflate(R.layout.fragment_one_asm5, container, false)
        edtId = view.findViewById(R.id.edtID)
        edtName = view.findViewById(R.id.edtName)
        edtPrice = view.findViewById(R.id.edtPrice)
        edtDescription = view.findViewById(R.id.edtDescription)
        imgProduct = view.findViewById(R.id.imgProduct)
        btnAdd = view.findViewById(R.id.btnAdd)

        var id = edtId!!.text.toString().toInt()
        var name = edtName!!.text.toString()
        var pPrice = edtPrice!!.text.toString()
        var pDes = edtDescription!!.text.toString()

        val p = Product(id, R.drawable.img1, name, pPrice, pDes)


        return view
    }
}