open class Person (name: String, male: String, date: String, address: String){
    var name : String = " "
    var male : String = " "
    var date : String = " "
    var address : String = " "

    init {
        this.name = name
        this.male = male
        this.date = date
        this.address = address
    }

    fun setInfo(name: String, male: String, date: String, address: String){
        this.name = name
        this.male = male
        this.date = date
        this.address = address
    }

    open fun getInfo(): String{
        return "Name: " + name + "Male" + male + "Date: " + date +
                "Address: " + address
    }


    open fun setInfo(
        name: String,
        male: String,
        date: String,
        address: String,
        MSSV: Int,
        email: String,
        MediumScore: Double
    ) {
    }

    open fun setInfo(name: String, male: String, date: String, address: String, nameClass: String, salary: Int) {}
}