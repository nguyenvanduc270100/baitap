class findMax<T>{

// Viết một phương thức generic để tìm phần tử max trong một phạm vi [begin, end) của một list
fun <T> MutableList<T>.max():T {
    var mMax = this[0]
    for(i in 1 .. this.size){
        if (mMax.toString().toInt() < this[i].toString().toInt()) {
            mMax = this[i]
        }
    }
    return mMax
}
}








