package com.nvd.android_asm23.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.nvd.myapplication.MainActivity
import com.nvd.myapplication.R


class ActivityB : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_b)

        // Bài 1
//        val inten = intent
//        val result = inten.getExtras()
//        val kq = result?.getStringArray("data")
//        Log.d("result", result.toString())

 // ==================================================

        // bài 2
//        val inten = intent
//        var a = inten.getIntExtra("number_one", 0)
//        var b = inten.getIntExtra("number_two", 0)
//        var c = a+b
        // gửi lại kết quả sang activity A
//        val btnResult = findViewById<Button>(R.id.btnResult)
//        btnResult.setOnClickListener {
//            val intent = Intent(this, ActivityA::class.java)
//            intent.putExtra("data_back", c)
//
//            // truyền dữ liệu ngược lại
//            setResult(RESULT_OK, intent)
//            finish()
//        }

    }
}