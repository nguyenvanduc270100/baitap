fun main(args: Array<String>) {
    val input = readLine()
    var a = input?.toInt()
    a?.DecimalToHexExample(a)


}
/*
1. Tạo một extension function của Int để chuyển đổi giá
trị số nguyên sang chuỗi thập lục phân.

 */
fun Int.DecimalToHexExample(a: Int): Unit {
    var rem:Int
    var b = a
    var str2: String? = " "
    var arr = arrayOf('0','1','2','3','4',
                '5','6','7','8','9','A','B','C','D','E','F' )
    while(b>0)
    {
        rem=b%16
        str2=arr[rem]+ str2!!
        b = b / 16
    }

    println("Method 2: Decimal to hexadecimal: "+str2)
}

/*
2. Tạo một extension function để chuyển đổi một byte
của chuỗi thập lục phân sang chuỗi nhị phân. Ví dụ:
val result = “C8”.toBinaryString()
 */
fun String.hexToBin(hex: String): String {
    var bin : String = ""
    var binFragment : String? = " "
    var myHex = hex
    var iHex : Int
    myHex = hex.trim()
    myHex = hex.replaceFirst("0x", "")
    for(i in 0 until  myHex.length){
        iHex = Integer.parseInt(""+myHex.get(i),16);
        binFragment = Integer.toBinaryString(iHex);

        while(binFragment?.length!! < 4){
            binFragment = "0" + binFragment;
        }
        bin += binFragment;
    }
    return bin
}
