fun main(args: Array<String>) {
    val q = Queue<Int>(100)

    q.enqueue(1)
    q.enqueue(2)

    q.peek()
    q.enqueue(3)
    q.dequeue()

    println("${q.isQueueEmpty()}")
}