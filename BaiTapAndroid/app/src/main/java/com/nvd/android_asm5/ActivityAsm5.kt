package com.nvd.android_asm5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.nvd.myapplication.R

class ActivityAsm5 : AppCompatActivity() {
    lateinit var frameOne : FrameLayout
    lateinit var frameTwo : FrameLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asm5)
        frameOne = findViewById(R.id.contenerOne)
        frameTwo = findViewById(R.id.contenerTwo)

        replace(R.id.contenerOne, FragmentOneAsm5())
        replace(R.id.contenerTwo, FragmentTwoAsm5())
    }

    fun replace( view : Int, frg : Fragment){
        val transition = supportFragmentManager.beginTransaction()
        transition.replace(view, frg)
        transition.commit()
    }
}