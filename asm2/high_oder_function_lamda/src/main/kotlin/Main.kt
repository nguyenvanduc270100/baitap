fun main(args: Array<String>) {
    var a = readLine()?.toInt()
    if (a != null){
        var b =  sumOfNumber(a)
        println(b)
    }
}

/*
bài 1
 Viết chương trình nhập vào một số rồi kiểm tra xem số đó có thể là
tổng của hai số nguyên tố hay không? Sau đó in ra những cặp số thỏa mãn. Ví dụ:
18 = 17+1, 18 =15+3, 18 = 11+7
 */

fun checkPrime(input: Int):Boolean{

        var count: Int = 0
        for (i in 1 .. input){
            if (input / i == 0){
                count++
            }
        }

        return count == 2

}

fun requestTopic(
    input: Int,
    checkPrime: (a: Int) -> Boolean
): String{
    var result: String = " "
    for (i in 1 .. input/2){
        if (checkPrime(i) && checkPrime(input -i)){
            result = result + ", $i + ${(input - i)}"
        }
    }
    return result
}


/*
bài 2
 2. Viết chương trình nhập vào hai số
 rồi tìm ra các số nguyên tố ở giữa hai số đó. Ví dụ:
number1 = 10, number2 = 20 thì các số nguyên tố ở giữa
hai số đó là 11, 13, 17, 19
 */

fun findPrime(a : Int, b: Int): String{
    var result = " "
    for (i in a until b){
        if (checkPrime(i)){
            result = "$result$i, "
        }
    }
    return  result
}

/*
bài 3
 3. Viết chương trình nhập vào một số rồi tìm tổng các số tự
 nhiên nhỏ hơn hoặc bằng số đó bằng đệ quy. Ví dụ nhập vào 5
 thì sẽ tính tổng 5+4+3+2+1 = 15
 */

fun findSum(input: Int): Int{
    if (input > 0){
        return input + findSum(input -1)
    }
    return 0
}

/*
bài 4
 Viết chương trình nhập vào một số tự nhiên rồi tính tổng
 các chữ số của nó. Ví dụ nhập vào 12345 thì sẽ tính tổng
 1+2+3+4+5 = 15
 */

fun sumOfNumber(a:Int): Int{

    var sum = 0
    var b = a

    while (b > 0){
        sum = sum + (b % 10)
        b = b / 10

    }
    return sum
}