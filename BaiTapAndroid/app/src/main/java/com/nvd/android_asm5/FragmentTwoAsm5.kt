package com.nvd.android_asm5

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nvd.myapplication.R

class FragmentTwoAsm5 : Fragment() {

    lateinit var rcv : RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_two_asm5, container, false)
        rcv = view.findViewById(R.id.listProduct)

        rcv.layoutManager = LinearLayoutManager(context)
        //val adap = AdapterProduct(getList(), requireContext())

        return view
    }

    private fun getList(): ArrayList<Product> {
            val list = ArrayList<Product>()
            return list
    }
}