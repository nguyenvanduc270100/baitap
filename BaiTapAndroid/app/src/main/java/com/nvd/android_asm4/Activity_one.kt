package com.nvd.android_asm4

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.nvd.myapplication.R

class Activity_one : AppCompatActivity() {
    private var txtTitle : TextView? = null
    private var edtAccount : EditText? = null
    private var edtName : EditText? = null
    private var edtSex : EditText? = null
    private var edtAge : EditText? = null
    private var btnConfirm : Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_linear_activity_one)

        txtTitle = findViewById(R.id.title)
        edtAccount = findViewById(R.id.edtAccount)
        edtName = findViewById(R.id.edtName)
        edtSex = findViewById(R.id.edtSex)
        edtAge = findViewById(R.id.edtAge)
        btnConfirm = findViewById(R.id.btnConfirm)

        btnConfirm!!.setOnClickListener {
            val intent : Intent = Intent(this, Activity_two::class.java)
            intent.putExtra("account", edtAccount!!.text)
            intent.putExtra("name", edtName!!.text)
            intent.putExtra("sex", edtSex!!.text)
            intent.putExtra("age", edtAge!!.text)
            startActivity(intent)
        }



    }
}