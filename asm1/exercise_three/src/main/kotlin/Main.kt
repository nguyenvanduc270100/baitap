fun main(args: Array<String>) {


    // Nhập một mảng các số nguyên a 0, a 1, a 2, ..., a n-1.
    // Không sử dụng bất kỳ mảng khác, in ra màn hình mảng trên
    // theo thứ tự tăng dần
    var M:IntArray= IntArray(10)
    M[0] = 5
    M[1] = 10
    M[2] = -4
    M[3] = 0
    M[4] = -2
    M[5] = 1
    M[6] = 9
    M[7] = 11
    M[8] = 20
    M[9] = 15
    M.sort()
    println("Tăng dần:")
    for (i in M)
        print("$i\t")
}