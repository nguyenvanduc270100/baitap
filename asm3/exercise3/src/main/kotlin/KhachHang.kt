class KhachHang(mount: Int, price: Int) {
    var mount = 0
    var price = 0
    init {
        this.mount = mount
        this.price = price
    }
    fun LoaiA(): Int{
        var money = mount * price + (mount*price)*10/100
        return money
    }

    fun LoaiB(sale: Int): Int{
        var money = (mount * price)*(100-sale)/100 + (mount*price)*10/100
        return money
    }

    fun LoaiC(): Int{
        var money = (mount * price)*50/100 + (mount*price)*10/100
        return money
    }
}