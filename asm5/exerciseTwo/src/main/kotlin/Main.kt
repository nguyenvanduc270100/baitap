fun main(args: Array<String>) {
//Khai báo 1 mảng có n phần tử các số nguyên,
// viết hàm nhập các phần tử cho mảng. Bắt ngoại lệ nếu nhập phần từ có
// giá trị là 100 thì in ra các phần tử đã nhập và kết thúc chương trình.
    val a:IntArray = intArrayOf(10)
    try {
        nhap(a)
    }catch (e:Exception){
        e.printStackTrace()
    }
    xuat(a)
}
fun nhap(a: IntArray) {
    for (i in 0 until a.size) {
        val my_input = readLine()?.toInt()
        my_input?.let {

            if (my_input == 100) {
                throw Exception("nhập phần tử có giá trị 100")
            } else {
                a[i] = my_input
            }
        }
    }
}

fun xuat(a: IntArray){
    for (i in a.indices){
        print("${a[i]},")
    }
}





