package com.nvd.android_asm23.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.util.Log
import com.nvd.android_asm23.activity.ActivityB

class MyBroadCast : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val result = intent!!.getStringExtra("data")
        Log.d("aa", result.toString())

        val i = Intent(context, ActivityB::class.java)
        i.flags = FLAG_ACTIVITY_NEW_TASK
        context!!.startActivity(i)

    }
}


