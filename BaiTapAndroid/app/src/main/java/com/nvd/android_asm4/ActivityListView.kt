package com.nvd.android_asm4

import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.nvd.myapplication.R

class ActivityListView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_listview)

        val listView = findViewById<ListView>(R.id.listView)
        val list = getListProduct()
        val adapter = AdapterListView(list)
        listView.adapter = adapter
    }

    fun getListProduct() : ArrayList<Product>{
        val list = ArrayList<Product>()
        list.add(Product(R.drawable.image_one, "product one ", "10000 VND"))
        list.add(Product(R.drawable.image_two, "product two ", "20000 VND"))
        list.add(Product(R.drawable.image_three, "product three ", "30000 VND"))
        list.add(Product(R.drawable.image_four, "product four ", "40000 VND"))
        list.add(Product(R.drawable.image_five, "product five ", "50000 VND"))
        return list
    }
}