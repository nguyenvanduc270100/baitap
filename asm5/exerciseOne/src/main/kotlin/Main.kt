import java.awt.geom.FlatteningPathIterator

fun main(args: Array<String>) {
    // Bài 1. Viết chương trình nhập vào 2 số thực.
// Bắt ngoại lệ để khi nhập vào không phải là số.
// Cài đặt hàm chia, trong đó bắt ngoại lệ nếu số chia
// là 0 thì thông báo phép chia không hợp lệ và kết thúc chương trình.
    val a = readLine()?.toFloat()
    val b = readLine()?.toFloat()
    val chia = a?.let { b?.let { it1 -> divNumber(it, it1) } }

    println(chia)
}

fun divNumber(paraOne: Float, paraTwo: Float): Float{

    var result: Float = 0F
    try {
        result = paraOne/paraTwo
    } catch (e:Exception){
        println(e.message)
    }

    return result
}