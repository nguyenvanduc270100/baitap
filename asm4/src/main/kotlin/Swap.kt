class Swap<F>{
//  Viết một phương thức generic để hoán đổi vị trí 2 phần tử khác nhau trong một mảng

    fun <T> MutableList<T>.swap(index1: Int, index2: Int) {
        val tmp = this[index1]
        this[index1] = this[index2]
        this[index2] = tmp
    }
}