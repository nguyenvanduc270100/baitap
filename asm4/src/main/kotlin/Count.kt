class Count<T>{

// Bài 1: Viết một phương thức generic để đếm số phần tử trong một tập hợp có thuộc tính cụ thể (tập số lẻ, tập số nguyên tố...)
    fun resultCount(arr: List<T>):Int{
        var my_count = 0
        for (i in 0 .. arr.size){
            my_count++
        }

        return my_count
    }

}