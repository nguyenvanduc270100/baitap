package com.nvd.android_asm4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.nvd.myapplication.R

class AdapterListView(list : ArrayList<Product>) : BaseAdapter() {
    var mList = list
    override fun getCount(): Int {
        TODO("Not yet implemented")
        return mList.size
    }

    override fun getItem(position: Int): Any {
        TODO("Not yet implemented")
        return mList.get(position)
    }

    override fun getItemId(position: Int): Long {
        TODO("Not yet implemented")

    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val convertView = LayoutInflater.from(parent!!.context).inflate(R.layout.layout_item, parent, false)
        val img = convertView.findViewById<ImageView>(R.id.image)
        val name = convertView.findViewById<TextView>(R.id.name)
        val price = convertView.findViewById<TextView>(R.id.price)

        val product = mList.get(position)
        img.setImageResource(product.Label!!)
        name.text = product.Name
        price.text = product.Price

        return convertView
    }
}


