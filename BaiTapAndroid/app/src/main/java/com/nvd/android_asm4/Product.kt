package com.nvd.android_asm4

import android.widget.ImageView
import android.widget.TextView

class Product {
    private var label : Int? = null
    private var name  : String? = null
    private var price : String? = null

    constructor(label : Int, name  : String,price : String ){
        this.label = label
        this.name = name
        this.price = price
    }

    var Label : Int?
    get() {return label}
    set(value) {label = value}

    var Name : String?
    get() {return name}
    set(value) {name  = value}

    var Price : String?
    get() {return price}
    set(value) {price = value}
}