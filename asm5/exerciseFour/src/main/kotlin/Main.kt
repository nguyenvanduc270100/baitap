fun main(args: Array<String>) {
    //Khai báo 2 mảng colors và colors2 và thêm các phần tử cho 2
// mảng đó. Tạo mới 2 List list và list2 có các phần tử là phần tử của
// 2 mảng vừa khai báo. Thêm các phần tử có trong list2 vào trong list,
// sau đó đưa list2 trở về 1 danh sách rỗng (không có phần tử nào) và
// hiển thị list. Chuyển các phần tử có trong list thành ký tự in hoa và
// hiển thị lại list đó. Xóa các phần tử trong list từ phần tử thứ 4 đến
// phần tử thứ 6. Hiển thị lại list sau khi xóa. Đảo ngược list và hiển
// thị lại list sau khi đảo ngược.

    val colors : CharArray = charArrayOf('a', 'b', 'c', 'd', 'e')
    val colors2 : CharArray = charArrayOf('w', 'r', 't', 'y')
    var list2: MutableList<Char> = mutableListOf('w', 'r', 't', 'y')
    var list:MutableList<Char> = mutableListOf('a', 'b', 'c', 'd', 'e')
    list.addAll(list2)
    list2.clear()

    println("hiển thị list 2 sau khi xóa")
    for(i in list2.indices){

        if (list2.size == 0){
            println("mãng rỗng")
            break

        }else{
            print("${list2[i]}\t")
        }
    }

    // chuyển sang ký tự hoa
    println("chuyển sang ký tự in hoa")
    for (i in list.indices){
        list[i] = list[i].toUpperCase()
    }
    // hiển thị lại
    for (i in list.indices){
        print("${list[i]}, ")
    }

    // xóa từ 4 đến 6
    for (i in 4..6){
        list.removeAt(i)
    }
    // hiển thị lại
    println("\nhiển thị list  sau khi xóa")
    for (i in list.indices){
        print("${list[i]}, ")
    }

    list.reverse()
    println("\nhiển thị list  sau khi đảo ")
    // hiển thị lại
    for (i in list.indices){
        print("${list[i]}, ")
    }


}