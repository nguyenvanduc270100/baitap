class Student (name: String, male: String,
               date: String, address: String,
                MSSV : Int, email : String,
                MediumScore : Double)
    : Person(name, male, date, address) {

    var MediumScore: Double
    var email: String
    var MSSV: Int

    init{
            this.name = name
            this.male = male
            this.date = date
            this.address = address
            this.MSSV = MSSV
            this.email = email
            this.MediumScore = MediumScore
        }

    override fun setInfo(name: String, male: String, date: String, address: String, MSSV : Int, email : String,
                         MediumScore : Double){
        this.name = name
        this.male = male
        this.date = date
        this.address = address
        this.MSSV = MSSV
        this.email = email
        this.MediumScore = MediumScore
    }

    override fun getInfo(): String{
        return "Name: " + name + "\tMale" + male + "\tDate: " + date +
                "\tAddress: " + address + "\nMSSV: " + MSSV + "\tEmail: " +
                email + "\tMediumScore: " + MediumScore
    }

    fun checkSholar(score: Double): Boolean{
        return score >= 8.0
    }

}