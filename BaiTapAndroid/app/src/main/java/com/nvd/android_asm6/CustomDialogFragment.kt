package com.nvd.android_asm6

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.nvd.myapplication.R

class CustomDialogFragment : DialogFragment() {
    var ratingRadioGroup : RadioGroup? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView : View = inflater.inflate(R.layout.dialog_fragment, container, false)
        var btnCancel = rootView.findViewById<Button>(R.id.btnCancel)
        var btnSubmit = rootView.findViewById<Button>(R.id.btnSubmit)
        ratingRadioGroup = rootView.findViewById(R.id.radioGroup)
        btnCancel.setOnClickListener {
            dismiss()
        }

        btnSubmit.setOnClickListener {
            val selectedId = ratingRadioGroup!!.checkedRadioButtonId
            val radio = rootView.findViewById<RadioButton>(selectedId)
            var ratingResult = radio.text.toString()

            Toast.makeText(context, "you rated: ${ratingResult}", Toast.LENGTH_LONG).show()
            dismiss()
        }
        return rootView
    }
}