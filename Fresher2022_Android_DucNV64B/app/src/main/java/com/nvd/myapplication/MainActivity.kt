package com.nvd.myapplication

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    // project b nhận dữ liệu từ project a và hiên thị lên textView
    // MY_ACTION và MY_DATA ở 2 project phải giống nhau
    // đăng ký nhận broadcast trong onStart và hủy trong onDestroy
    val MY_ACTION = "com.nvd.DucNV64"
    val MY_DATA = "DucNV64"
    var result : TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        result = findViewById<TextView>(R.id.result)

    }

    var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (MY_ACTION.equals(intent.action)) {
                var text = intent.getStringExtra(MY_DATA)
                result!!.text = text
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter(MY_ACTION)
        registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
    }
}