package com.nvd.myapplication

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.nvd.myapplication.NotifiChannelid.Companion.CHANNEL_ID

class MyService : Service() {

    override fun onCreate() {
        super.onCreate()
        createNotification()
    }

    private fun createNotification() {


        val builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Android final test")
            .setContentText("sum from 1 to 1000000")

        startForeground(1, builder.build())

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        var sum = 0
        for (i in 1 .. 1000000){
            sum += 1
        }

        Thread.sleep(5000)

        val i = Intent("MY_ACTION")
        i.putExtra("MY_KEY", sum.toString())
        sendBroadcast(i)
        //Log.d("aa", "$sum")
        return START_NOT_STICKY
    }
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}