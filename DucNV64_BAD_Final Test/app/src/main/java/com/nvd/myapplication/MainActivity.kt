package com.nvd.myapplication

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

// use foreground service to do
// use broadcast to communicate between service and activity, send result from service to activity

class MainActivity : AppCompatActivity() {
    var mBtnStart : Button? = null
    var mBtnStop : Button? = null

    private val mBroadCast: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if ("MY_ACTION" === intent.action) {
                val i = intent.getStringExtra("MY_KEY")

                val alertDialogBuilder = AlertDialog.Builder(this@MainActivity)
                alertDialogBuilder.setTitle("Service completed")
                alertDialogBuilder.setMessage("sum from 1 to 1000000 is $i")
                alertDialogBuilder.setCancelable(true)

                val alertDialog = alertDialogBuilder.create()
                alertDialog.show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter("MY_ACTION")
        registerReceiver(mBroadCast, intentFilter)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(mBroadCast)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mBtnStart = findViewById(R.id.start)
        mBtnStop = findViewById(R.id.stop)

        mBtnStart!!.setOnClickListener {
            val i = Intent(this, MyService::class.java)
            startService(i)
        }

        mBtnStop!!.setOnClickListener {
            val i = Intent(this, MyService::class.java)
            stopService(i)
        }

    }


}