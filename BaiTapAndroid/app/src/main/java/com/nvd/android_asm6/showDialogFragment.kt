package com.nvd.android_asm6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.nvd.myapplication.R

class showDialogFragment : AppCompatActivity() {
    var btn : Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_dialog_fragment)

        btn = findViewById(R.id.button)

        btn!!.setOnClickListener {
            val dl = CustomDialogFragment()
            dl.show(supportFragmentManager, "dialog")
        }
    }
}