class Queue<T> {
    private var maxSize = 0
    private var queueArray: Array<T>
    private var front = 0
    private var rear = 0
    private var currentSize = 0


    constructor(size: Int) {
        maxSize = size
        this.queueArray = emptyArray<T>()
        front = 0
        rear = -1
        currentSize = 0
    }


    fun enqueue(item: T) {
        /* Checks whether the queue is full or not */
        if (isQueueFull()) {
            println("Queue is full!")
            return
        }
        if (rear == maxSize - 1) {
            rear = -1
        }
        /* increment rear then insert element to queue */
        queueArray[++rear] = item
        currentSize++
        println("Item added to queue: $item")
    }


    fun dequeue(): T {
        /* Checks whether the queue is empty or not */
        if (isQueueEmpty()) {
            throw RuntimeException("Queue is empty")
        }
        /* retrieve queue element then increment */
        val temp = queueArray[front++]
        if (front == maxSize) {
            front = 0
        }
        currentSize--
        return temp
    }

    /* Queue:Peek Operation */
    fun peek(): T {
        return queueArray[front]
    }

    /* Queue:isFull Operation */
    fun isQueueFull(): Boolean {
        return maxSize == currentSize
    }

    /* Queue:isEmpty Operation */
    fun isQueueEmpty(): Boolean {
        return currentSize == 0
    }

}