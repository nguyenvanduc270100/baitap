package com.nvd.android_asm4

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.nvd.myapplication.R

class Activity_two : AppCompatActivity() {
    private var txtTitle : TextView? = null
    private var edtAccount : TextView? = null
    private var edtName : TextView? = null
    private var edtSex : TextView? = null
    private var edtAge : TextView? = null
    private var btnOk : Button? = null
    private var btnCancel : Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_linear_activity_two)

        txtTitle = findViewById(R.id.title)
        edtAccount = findViewById(R.id.edtAccount)
        edtName = findViewById(R.id.edtName)
        edtSex = findViewById(R.id.edtSex)
        edtAge = findViewById(R.id.edtAge)
        btnOk = findViewById(R.id.btnOk)
        btnCancel = findViewById(R.id.btnCancel)

        val i = intent
        val name = i.getStringExtra("name")
        val account = i.getStringExtra("account")
        val age = i.getStringExtra("age")
        val sex = i.getStringExtra("sex")

        txtTitle!!.text = "Chào mừng " + name
        edtAccount!!.text = account
        edtName!!.text = name
        edtAge!!.text = age
        edtSex!!.text = sex

        btnCancel!!.setOnClickListener { finish() }
        btnOk!!.setOnClickListener {
              val intent = Intent(applicationContext, Activity_one::class.java)
                startActivity(intent)
        }
    }
}