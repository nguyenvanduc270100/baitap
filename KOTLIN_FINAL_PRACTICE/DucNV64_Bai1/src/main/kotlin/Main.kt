fun main(args: Array<String>) {
    val str = readLine()
    val input = str?.trim()

    var result:Boolean = false
    if (input != null)
       result = palindrome(input)

    if(result == false){
        println("This is not palindrome")
    } else{
        println("This is palindrome")
    }
}

fun palindrome(input:String):Boolean{

    for(i in 0 .. input.length/2){
        if (input[i] != input[input.length - i -1])
        {
            return false
        }
    }
    return true
}