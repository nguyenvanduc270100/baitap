fun main(args: Array<String>) {


    //  Viết chương trình nhập vào một số nguyên có hai chữ số.
    //  Chuyển đổi và in ra giá trị của số đã nhập ở dạng nhị phân
    //  và thập lục phân.


    val input:String?= readLine()
    var a:Int = input!!.toInt()
    var result: Int = 0
    while (a != 0){
        var b = a / 2
        a = a % 2
        result = result *10 + b
    }
    // print the result in the reverse
    // direction will get the result in binary
    var kq:String = result.toString()
    println(kq.reversed())


}