import java.util.*
import kotlin.collections.ArrayList

class Stack{
    private lateinit var arr: IntArray
    private var size = 0
    private var index = 0

    constructor(size: Int) {
        this.size = size
        arr = IntArray(size)
    }

    fun push(element: Int) {
        if (isFull()) {
            throw StackOverflowError("Stack is full")
        }
        arr[index] = element
        index++
    }

    fun pop(): Int? {
        if (isEmpty()) {
            return null
        }
        return arr[--index]
    }

    fun isEmpty(): Boolean {
        return if (index == 0) {
            true
        } else false
    }

    fun isFull(): Boolean {
        return if (index == size) {
            true
        } else false
    }

    fun size(): Int {
        return index
    }
}