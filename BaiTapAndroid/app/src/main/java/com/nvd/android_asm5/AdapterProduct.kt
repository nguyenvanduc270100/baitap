package com.nvd.android_asm5

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nvd.android_asm4.Product
import com.nvd.myapplication.R

class AdapterProduct(private var listProduct: ArrayList<Product>, ctx: Context) :
    RecyclerView.Adapter<AdapterProduct.ProductViewHolder>() {

    private var mCtx : Context? = ctx
    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var imgPro = itemView.findViewById<ImageView>(R.id.imgPro)
        var txtName = itemView.findViewById<TextView>(R.id.nameProduct)
        var txtPrice = itemView.findViewById<TextView>(R.id.priceProduct)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.product_item_asm5, parent, false)

        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val p = listProduct.get(position)
        holder.imgPro.setImageResource(R.drawable.img1)
        holder.txtName.text = p.Name
        holder.txtPrice.text = p.Price
    }

    override fun getItemCount(): Int {
        return listProduct.size
    }
}