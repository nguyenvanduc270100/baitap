class Teacher(name: String, male: String,
              date: String, address: String,
                nameClass: String, salary: Int)
    : Person(name, male, date, address) {

        var nameClass: String = " "
        var salary : Int  = countSalary(5, 'G')
    override fun setInfo(name: String, male: String, date: String, address: String,
                         nameClass: String, salary: Int){
        this.name = name
        this.male = male
        this.date = date
        this.address = address
        this.nameClass = nameClass
        this.salary = salary

    }

    override fun getInfo(): String{
        return "Name: " + name + "\tMale" + male + "\tDate: " + date +
                "\tAddress: " + address + "\nnameClass: " + nameClass + "\tsalary: " +
                salary
    }

    fun countSalary(hourTeaching: Int, kip: Char): Int{
        var mSalary = 0
        if (kip == 'K' || kip == 'H' || kip == 'I' || kip == 'G'){
            mSalary = 80 * hourTeaching
        } else if (kip == 'L' || kip == 'M'){
            mSalary = 80 * hourTeaching + 500
        }
        return mSalary
    }
}