fun main(args: Array<String>) {
    var KhachLoaiA = KhachHang(5, 1000)
    var KhachLoaiB = KhachHang(10, 2000)
    var KhachLoaiC = KhachHang(15, 1500)

    println("khách loại A phải trả ${KhachLoaiA.LoaiA()}")
    println("khách loại B phải trả ${KhachLoaiB.LoaiB(50)}")
    println("khách loại C phải trả ${KhachLoaiC.LoaiC()}")
    println("tổng tiền ${KhachLoaiA.LoaiA()} + ${KhachLoaiB.LoaiB(50)}" +
            " + ${KhachLoaiC.LoaiC()}")
}